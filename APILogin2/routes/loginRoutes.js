const express = require("express");
const Router = express.Router();
const loginController = require("../controller/loginController");
const loginModel = require("../model/loginModel");

// Router.post("/login", companiesRoutes.Login);
// Router.post('/loginModel',loginModel.required())
Router.post('/login', loginController.Login);
//colocado depois
//Router.get('/login', loginController.getLogin);
module.exports = Router;
