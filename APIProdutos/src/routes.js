const express = require('express');
const productController = require('./controllers/productController');

const routes = express.Router();


routes.get('/products', productController.index)

module.exports = routes;