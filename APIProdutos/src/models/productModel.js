const { Model, DataTypes } = require('sequelize');

class Products extends Model {
    static init(sequelize) {
        super.init({
            name: DataTypes.STRING,
            description: DataTypes.STRING,
            image: DataTypes.STRING
        },{
            sequelize
        })
    }
}
module.exports = Products;