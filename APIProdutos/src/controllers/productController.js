const Products = require('../models/productModel');

async function index(req, res) {
    const product = await Products.findAll()
    console.log(product)
    return res.json(product)
}

module.exports = { index };