const Sequelize = require('sequelize');
const dbConfig = require('../config/database.js');

const Products = require('../models/productModel');

const connection = new Sequelize(dbConfig);

Products.init(connection);

module.exports = connection;